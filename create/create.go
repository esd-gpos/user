package create

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"strings"
	"sync"
	"unicode"
)

var (
	// errors for validateUsername function
	errorShortName         = errors.New("username too short - length should be [4, 16]")
	errorLongName          = errors.New("username too long - length should be [4, 16]")
	errorRequireAlphaName  = errors.New("username requires at least one english alphabet character")
	errorInvalidSymbolName = errors.New("username cannot contain symbol")

	// errors for validatePassword function
	errorShortPassword         = errors.New("password too short - length should be [4,16]")
	errorLongPassword          = errors.New("password too long - length should be [4, 16]")
	errorRequireNumPassword    = errors.New("password requires at least one number")
	errorRequireAlphaPassword  = errors.New("password requires at least one english alphabet character")
	errorInvalidSymbolPassword = errors.New("password cannot contain symbol")

	// errors for MongoCreateUser interface
	errorDuplicateUser = errors.New("user already exist")
	errorInternalMongo = errors.New("internal mongo error")

	// error for constructor
	errorAddingValidator = errors.New("could not set validator")
)

func validateUsername(username string) error {
	length := 0
	containAlpha := false

	for _, c := range username {
		if unicode.IsLetter(c) {
			containAlpha = true
		} else if !unicode.IsNumber(c) {
			return errorInvalidSymbolName
		}

		length += 1
	}

	// check length
	if length < 4 {
		return errorShortName
	}

	if length > 16 {
		return errorLongName
	}

	// check require alphabet
	if !containAlpha {
		return errorRequireAlphaName
	}

	return nil
}

func validatePassword(password string) error {
	length := 0
	containAlpha := false
	containNum := false

	for _, c := range password {
		if unicode.IsLetter(c) {
			containAlpha = true
		} else if unicode.IsNumber(c) {
			containNum = true
		} else {
			return errorInvalidSymbolPassword
		}

		length += 1
	}

	// check length
	if length < 4 {
		return errorShortPassword
	}

	if length > 16 {
		return errorLongPassword
	}

	// check require alphabet
	if !containAlpha {
		return errorRequireAlphaPassword
	}

	// check require number
	if !containNum {
		return errorRequireNumPassword
	}

	return nil
}

// Register api version 1 routes for creating user
func RegisterCreateRoutesV1(engine *gin.Engine, m MongoCreateUser) error {
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterStructValidation(UserStructLevelValidation, User{})
	} else {
		return errorAddingValidator
	}

	group := engine.Group("/api/v1/user")
	group.Use(MongoCreateContextHandler(m))
	group.POST("", PostUserHandler)

	return nil
}

// User model struct
type User struct {
	Email    string                 `json:"email"`
	Username string                 `json:"username" binding:"required"`
	Password string                 `json:"password" binding:"required"`
	Verified bool                   `json:"verified"`
	Active   bool                   `json:"active"`
	Settings map[string]interface{} `json:"settings"`
}

// Add struct level validation to User struct
func UserStructLevelValidation(sl validator.StructLevel) {
	user := sl.Current().Interface().(User)

	err := validateUsername(user.Username)
	if err != nil {
		sl.ReportError(user.Username, "username", "Username", "username", "")
	}

	err = validatePassword(user.Password)
	if err != nil {
		sl.ReportError(user.Password, "password", "Password", "password", "")
	}

	if len(user.Email) > 254 {
		sl.ReportError(user.Email, "email", "Email", "email", "")
	}
}

// Create User handler
func PostUserHandler(ctx *gin.Context) {
	var user User
	if err := ctx.ShouldBindJSON(&user); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": "User validation fails", "error": err.Error()})
		return
	}

	// set other values for user struct
	user.Settings = make(map[string]interface{})
	user.Verified = false
	user.Active = true

	temp, ok := ctx.Get("mongo")
	if !ok {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "something went wrong on our end", "error": "server error"})
		return
	}

	m := temp.(MongoCreateUser)
	user.Email = strings.TrimSpace(user.Email)

	if err := m.Insert(ctx, user); err != nil {
		if err == errorDuplicateUser {
			ctx.JSON(http.StatusConflict, gin.H{"message": "could not create user", "error": errorDuplicateUser})
			return
		}

		if err == errorInternalMongo {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": "could not create user", "error": "server error"})
			return
		}

		ctx.JSON(http.StatusInternalServerError, gin.H{"message": "something went wrong on our end", "error": "server error"})
		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": "user created successfully"})
}

// Adapter interface for our mongo client - used for insertion operation
// Will be set in the context in the middleware layer
type MongoCreateUser interface {
	Insert(context.Context, User) error
}

// Concrete Adapter for our MongoClient interface
type MongoCreateUserClient struct {
	Col *mongo.Collection
}

func (m *MongoCreateUserClient) Insert(ctx context.Context, user User) error {
	var wg sync.WaitGroup

	duplicated := false
	child, cancel := context.WithCancel(ctx)

	// check if email is in use
	if user.Email != "" {
		wg.Add(1)

		go func() {
			res := m.Col.FindOne(child, bson.M{"email": user.Email})
			if res.Err() != mongo.ErrNoDocuments {
				duplicated = true
				cancel()
			}

			wg.Done()
		}()
	}

	// check if username exist
	wg.Add(1)

	go func() {
		res := m.Col.FindOne(child, bson.M{"username": user.Username})
		if res.Err() != mongo.ErrNoDocuments {
			duplicated = true
			cancel()
		}

		wg.Done()
	}()

	// wait for the two query
	wg.Wait()

	if duplicated {
		return errorDuplicateUser
	}

	// insert document
	_, err := m.Col.InsertOne(ctx, user)
	if err != nil {
		return errorInternalMongo
	}

	return nil
}

// return handler to add MongoCreateUser to context
func MongoCreateContextHandler(m MongoCreateUser) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set("mongo", m)
	}
}
