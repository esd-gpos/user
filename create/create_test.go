package create

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestValidUsername(t *testing.T) {
	testCases := []struct {
		in  string
		out error
	}{
		{in: "hel", out: errorShortName},
		{in: "12345678901234567", out: errorLongName},
		{in: "1234567890", out: errorRequireAlphaName},
		{in: "$hello123", out: errorInvalidSymbolName},
		{in: "hello123", out: nil},
		{in: "hello", out: nil},
	}

	for _, c := range testCases {
		err := validateUsername(c.in)
		if err != c.out {
			log.Fatalf("expected = [%v], got = [%v]", c.out, err)
		}
	}
}

func TestValidPassword(t *testing.T) {
	testCases := []struct {
		in  string
		out error
	}{
		{in: "hel", out: errorShortPassword},
		{in: "12345678901234567", out: errorLongPassword},
		{in: "hello", out: errorRequireNumPassword},
		{in: "1234567890", out: errorRequireAlphaPassword},
		{in: "$hello123", out: errorInvalidSymbolPassword},
		{in: "hello123", out: nil},
	}

	for _, c := range testCases {
		err := validatePassword(c.in)
		if err != c.out {
			log.Fatalf("expected = [%v], got = [%v]", c.out, err)
		}
	}
}

func TestCreate(t *testing.T) {
	gin.SetMode(gin.ReleaseMode)
	engine := gin.New()

	_ = RegisterCreateRoutesV1(engine, &testCreateUserClient{})

	testCases := []struct {
		in  map[string]string
		out int
	}{
		{in: map[string]string{"username": "TestUser", "password": "TestPassword123"}, out: 201},
		{in: map[string]string{"email": "", "username": "TestUser", "password": "TestPassword123"}, out: 201},
		{in: map[string]string{"email": "test@gmail.com", "username": "TestUser", "password": "TestPassword123"}, out: 201},
		{in: map[string]string{"username": "ExistingUser", "password": "TestPassword123"}, out: 409},
		{in: map[string]string{"username": "", "password": ""}, out: 400},
		{in: map[string]string{"username": "TestUser"}, out: 400},
		{in: map[string]string{"password": "TestPassword123"}, out: 400},
	}

	for _, c := range testCases {
		res := httptest.NewRecorder()
		body, _ := json.Marshal(c.in)

		req := httptest.NewRequest(http.MethodPost, "/api/v1/user", bytes.NewReader(body))
		engine.ServeHTTP(res, req)

		if res.Code != c.out {
			t.Log(res.Body)
			t.Fatalf("expected = [%v], got = [%v]", c.out, res.Code)
		}
	}
}

// Struct implementing the MongoCreateUser interface - used in TestCreate
type testCreateUserClient struct {}

func (t* testCreateUserClient) Insert(ctx context.Context, user User) error {
	if user.Username == "ExistingUser" {
		return errorDuplicateUser
	}

	return nil
}
