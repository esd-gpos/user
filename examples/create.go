package main

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/esd-gpos/user/create"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

// Assuming that MongoDB is running on localhost - look at docker-compose file
func main() {
	// create client with options
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://mongo:password@localhost:27017"))
	failOnError("could not create mongodb client", err)

	// establish connection - non blocking operation
	err = client.Connect(context.TODO())
	failOnError("client could not connect to mongodb", err)

	exampleCollection := client.Database("user").Collection("example")
	userClient := &create.MongoCreateUserClient{Col: exampleCollection}

	// register the create user routes
	router := gin.Default()
	err = create.RegisterCreateRoutesV1(router, userClient)
	failOnError("could not register create user routes", err)

	log.Fatal(router.Run("127.0.0.1:8080"))
}

func failOnError(msg string, err error) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}
