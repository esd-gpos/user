# **DEPRECATED**

# user
Handles all access to the user database. Exposes functionality as REST. HTTP is the recommended method of interacting with this service externally.
When connecting externally, make sure to use HTTPS connection.

**HEADER**
All headers from authenticated user must contain the jwt token and all request body must be in JSON format.

| KEY           | VALUE               |
| ------------- | ------------------- |
| Content       | application/json    |
| Authorization | Bearer ${JWT_TOKEN} |

## HTTP API
### Basic usage
##### `api/v1/user/${USER_ID}`
There are 2 supported http request methods for interacting with authenticated user resource via user service (does not cover password reset, email/username change). The table below summarises basic usage.

| METHOD | BODY                                  |
| ------ | ------------------------------------- |
| GET    | No body                               |
| PATCH  | Key value pairs of info to be updated |

#### User registration
##### `api/v1/user`
This is for user creation

| METHOD | BODY                                                                                           |
| ------ | ---------------------------------------------------------------------------------------------- |
| POST   | {"email": string (**optional**), "username": string, "password1": string, "password2": string} |

#### User login
##### `api/v1/user/token`
Will return a jwt token that can be used to represent the user.

| METHOD | BODY                                     |
| ------ | ---------------------------------------- |
| POST   | {"username": string, "password": string} |


### Verify email
##### `api/v1/user/${USER_ID}/verified`
This will send a verification email to the user with a link (token attached to the url) that will can be clicked upon to change the user email to verified status. 

| METHOD | BODY    |
| ------ | ------- |
| PUT    | No body |

##### `api/v1/user/verified`

| METHOD | BODY                                  |
| ------ | ------------------------------------- |
| PUT    | {"token": string, "password": string} |

### Credentials reset/change
#### Password reset for authenticated user 
##### `api/v1/user/${USER_ID}/password`
This is meant for authenticated users who want to reset their password (include jwt token in header).

| METHOD | BODY                           |
| ------ | ------------------------------ |
| PUT    | {"old": string, "new": string} |

#### Password reset for unauthenticated user 
##### `api/v1/user/password`
This is meant for unauthenticated users who want to request password reset. This will sends an email to the user, with a link containing the reset token in the link, which must then be used to call the actual reset action.

| METHOD | BODY              |
| ------ | ----------------- |
| PUT    | {"email": string} |

##### `api/v1/user/password`
 To reset the password, call the same API with the token passed in instead of the old password.

| METHOD | BODY                                  |
| ------ | ------------------------------------- |
| PUT    | {"token": string, "password": string} |

#### Change email (Requires user to be authenticated)
##### `api/v1/user/${USER_ID}/email`
Support only one http request method.

| METHOD | BODY                                |
| ------ | ----------------------------------- |
| PUT    | {"new": string, "password": string} |

#### Change username (Requires user to be authenticated)
##### `api/v1/user/${USER_ID}/username`
Support only one http request method.

| METHOD | BODY                                |
| ------ | ----------------------------------- |
| PUT    | {"new": string, "password": string} |

### Deactivating account
#### Deactivating user account (Requires user to be authenticated)
##### `api/v1/user/${USER_ID}/state`
Support only one http method. Will change the `active` field of user account to false.

| METHOD | BODY                                    |
| ------ | --------------------------------------- |
| PUT    | {"password": string, "active": boolean} |

### Reactivating account
#### Reactivating user account
##### `api/v1/user/state`
Will first send an email to the user, with a link to confirm reactivation of account, with a token in the link that will be used to confirm the reactivation.

| METHOD | BODY              |
| ------ | ----------------- |
| PUT    | {"email": string} |

## Database document attributes
| NAME     | TYPE     | DESC                                              |
| -------- | -------- | ------------------------------------------------- |
| _id      | string   | automatically indexed by mongo                    |
| email    | string   | optional - doesn't need type checking - is unique |
| username | string   | identifies a user - is unique                     |
| password | string   | hashed password                                   |
| verified | boolean  | true if email is verified                         |
| active   | boolean  | true if user account is active                    |
| settings | document | nested type for any future settings               |